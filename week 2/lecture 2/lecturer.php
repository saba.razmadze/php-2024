<?php
    include "q_for_quiz.php";
    // echo "<pre>"; 
    //     print_r($questions);
    // echo "</pre>";
    // shuffle($questions);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grade</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <form action="lecturer.php" method="post">
            <h2>შეფასება -  <?= $_POST['firstName']." - ".$_POST['lastName']?></h2>
            <div class="row title">
                <div class="question">კითხვა </div>
                <div class="point">მაქსიმალური ქულა</div>
                <div class="answer">პასუხები</div>
                <div>შეფასება</div>
            </div>

            <?php
            $i=0;
            foreach ($questions as $items):
            ?>
            <div class="row">
                <div class="question"><?=$items['question']?></div>
                <div class="point"><?=$items['point']?></div>
                <div> <?= $_POST['answer'][$i]?> </div>
                <div><input type="text" placeholder="ქულა" size="10"></div>
            </div>
            
            <?php  $i++; endforeach ?>

            <div class="row">
                <label for="">ლექტორი</label>
                <div><input type="text" placeholder="სახელი"></div>
                <div><input type="text" placeholder="გვარი"></div>
                <button>გაგზავნა</button>
            </div>
        </form>
    </div>

</body>
</html>