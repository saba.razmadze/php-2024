<?php
    include "q_for_quiz.php";
    // echo "<pre>"; 
    //     print_r($questions);
    // echo "</pre>";
    // shuffle($questions)    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>QUIZ</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <form action="lecturer.php" method="post">
            <h2>ქვიზი</h2>
            <div class="row title">
                <div class="question">კითხვა </div>
                <div class="point">მაქსიმალური ქულა</div>
                <div>პასუხი</div>
            </div>

            <?php
            foreach ($questions as $items):
            ?>
            <div class="row">
                <div class="question"><?=$items['question']?></div>
                <div class="point"><?=$items['point']?></div>
                <div><input type="text" placeholder="პასუხი" name="answer[]"></div>
            </div>
            
            <?php endforeach ?>

            <div class="row">
                <label for="">სტუდენტი</label>
                <div><input type="text" placeholder="სახელი" name="firstName"></div>
                <div><input type="text" placeholder="გვარი" name="lastName"></div>
                <button>გაგზავნა</button>
            </div>
        </form>
    </div>

</body>
</html>