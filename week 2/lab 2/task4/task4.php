<?php
    $cars = array(
        array("Make"=>"Toyota", 
            "Model"=>"Corolla", 
            "Color"=>"White", 
            "Mileage"=>24000, 
            "Status"=>"Sold"),

        array("Make"=>"Toyota", 
            "Model"=>"Camry", 
            "Color"=>"Black", 
            "Mileage"=>56000, 
            "Status"=>"Available"),

        array("Make"=>"Honda", 
            "Model"=>"Accord", 
            "Color"=>"White", 
            "Mileage"=>15000, 
            "Status"=>"Sold")  
        );
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        foreach( $cars as $mancanebi ):
    ?>
        <div>
            <label>Make- <?=$mancanebi["Make"] ?></label><br>
            <label>Model- <?=$mancanebi["Model"] ?></label><br>
            <label>Color- <?=$mancanebi["Color"] ?></label><br>
            <label>Mileage- <?=$mancanebi["Mileage"] ?></label><br>
            <label>Status- <?=$mancanebi["Status"] ?></label><br><br>
        </div>    
    <?php
        endforeach
    ?>

    <table border="1" >
        <thead>
            <td>Make</td>
            <td>Model</td>
            <td>Color</td>
            <td>Mileage</td> 
            <td>Status</td>
        </thead>
        <tbody>
            <?php
                foreach( $cars as $mancanebi ):
            ?>
                <tr>
                    <td><?=$mancanebi["Make"] ?></td>
                    <td><?=$mancanebi["Model"] ?></td>
                    <td><?=$mancanebi["Color"] ?></td>
                    <td><?=$mancanebi["Mileage"] ?></td>
                    <td><?=$mancanebi["Status"] ?></td>
                </tr>    
            <?php
                endforeach
            ?>
        </tbody>
    </table>
</body>
</html>