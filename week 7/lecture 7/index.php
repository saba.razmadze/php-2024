<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">

    <nav>
        <ul>
           
            <li><a href="index.php">home</a></li>
            <li><a href="index.php?nav=quiz">quiz</a></li>
            <li><a href="index.php?nav=subjects">subjects</a></li>
            <li><a href="index.php?nav=fileAndFolder">file and folder</a></li>
            <li><a href="index.php?nav=upload">upload</a></li>
   
        </ul>
    </nav>
    <div class="container">
        <?php
            if(isset($_GET['nav'])&& $_GET[ 'nav'] == "quiz"){
                include 'quiz/index.php';
            }elseif(isset($_GET['nav'])&& $_GET['nav'] == 'lecturer'){
                include 'quiz/lecturer.php';
            }
  
            elseif(isset($_GET['nav'])&& $_GET['nav'] == 'subjects'){
                echo  "<h2>Subjects Page</h2>";
            }elseif(isset($_GET['nav'])&& $_GET['nav'] == 'fileAndFolder'){
                echo  "<h2>fileAndFolder Page</h2>";
            }elseif(isset($_GET['nav'])&& $_GET['nav'] == 'upload'){
                echo  "<h2>upload Page</h2>";
            }
            else{
            include "upload_file/index.php";
            }
        ?>
    </div>
</body>
</html>