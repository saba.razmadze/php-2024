<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>task3-exam</title>
</head>
<body>
    <form method="post">
        <h2>student's firstName - </h2>
            <input type="text" name="stfirstName">
            <br><br>
        <h2>student's lastName -</h2>
            <input type="text" name="stlastName">
            <br><br>
        <h2>semester -</h2>
            <select name="semester">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
            <br><br>

        <h2>course code -</h2>
            <input type="text" list="courseCodeList" name="courseCode">
            <datalist id="courseCodeList">
                <option value="100">100</option>
                <option value="101">101</option>
            </datalist >   
            <br><br>

        <h2>Garde</h2>
            <input type="number" name="Garde" min="0" max="100">
            <br><br>   

        <h2>lecturer's firstName - </h2>
            <input type="text" name="lefirstName">
            <br><br>
        <h2>lecturer's lastName -</h2>
            <input type="text" name="lelastName">
            <br><br>
        <h2>dean's FullName -</h2>
            <input type="text" name="deanFullName">
            <br><br>
        <button>check</button>
        <br><br>
    </form>
    <table border="1">
        <thead>
            <td>saxeli</td>
            <td>gvari</td>
            <td>kursi</td>
            <td>semestri</td>
            <td>nishani</td>
            <td>gadayvana</td>
            <td>leqtoris saxeli</td>
            <td>leqtoris gvari</td>
            <td>dekanis saxeli-gvari</td>
        </thead>
        <tbody>
            <tr>
                <td><?=$_POST['stfirstName']?></td>
                <td><?=$_POST['stlastName']?></td>
                <td><?=$_POST['semester']?></td>
                <td><?=$_POST['courseCode']?></td>
                <td><?=$_POST['Garde']?></td>
                <td>
                    <?php
                    $grad = $_POST['Garde']; 
                    $nishani = "";
                     if ($grad <=50) {
                        $nishani = 'chagrcha';
                    }
                    elseif ($grad <61) {
                        $nishani = 'E';
                    }
                    elseif ($grad <71) {
                        $nishani = 'D';
                    }
                    elseif ($grad <81) {
                        $nishani = 'C';
                    }
                    elseif ($grad <91) {
                        $nishani = 'B';
                    }
                    else{
                        $nishani = 'sawyalo';
                    }
                    echo $nishani;
                
                ?>
                </td>
                <td><?=$_POST['lefirstName']?></td>
                <td><?=$_POST['lelastName']?></td>
                <td><?=$_POST['deanFullName']?></td>
            </tr>
        </tbody>
    </table>
</body>
</html>