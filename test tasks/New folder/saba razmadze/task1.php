<?php
    $phone=$misamarti=$errorNameLast=$errorName='';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <div>
            <input type="text" name="name" placeholder="სახელი ">
            <span class="error"><?= $errorName?></span>
        </div>
        <div>
            <input type="text" name="namelast" placeholder="გვარი ">
            <span class="error"><?= $errorNameLast?></span>
        </div>
        <div>
            <input type="text" name="misamarti" placeholder="მისამართი ">
            <span class="error"><?= $misamarti?></span>
        </div>
        <div>
            <input type="text" name="number" placeholder="მობილური ">
            <span class="error"><?= $phone?></span>
        </div>
        <button>submit</button>
    </form>
    <?php
        if(isset($_POST['name'])  && empty($_POST['name']) && strlen($_POST['name'])<2 && strlen($_POST['name'])<20) { 
            $errorName='error name';
        } 
        if(isset($_POST['namelast'])  && empty($_POST['namelast']) && strlen($_POST['namelast'])<3 && strlen($_POST['namelast'])<50) { 
            $errorNameLast='errorNameLast';
        } 
        if(count_chars($_POST['misamarti'])>70) { 
            $misamarti='misamarti';
        } 
        if(is_string($_POST['number'])>20) { 
            $phone='phone';
        }
    ?>
</body>
</html>