<?php
    $error='';
    $filetext='';
    $size= 2*1024*1024;
    if (isset( $_POST['upload'])) {
        if(substr($_FILES['f_name']['name'], -3)=='txt' && $_FILES['f_name']['size']<$size){
            $patch = 'storage/'.$_FILES['f_name']['name'];
            move_uploaded_file($_FILES['f_name'][ 'tmp_name'],$patch);

            fopen($patch,'r');
            $filetext= fgets($patch,100000);
        }else{
            $error='not txt';
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post" enctype="multipart/form-data">
        <div>
            <input type="file" name="f_name">
            <label class="error"><?= $error?></label>
        </div>
        <div>
            <input type="submit" name="upload">
        </div>
    </form>
    <?php
    echo  $filetext;
    ?>
</body>
</html>