<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>task3-exam</title>
</head>
<body>
    <form action="task2Get.php" method="get">
        <h2>student's firstName - </h2>
            <input type="text" name="stfirstName">
            <br><br>
        <h2>student's lastName -</h2>
            <input type="text" name="stlastName">
            <br><br>
        <h2>semester -</h2>
            <select name="semester">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
            <br><br>

        <h2>course code -</h2>
            <input type="text" list="courseCodeList" name="courseCode">
            <datalist id="courseCodeList">
                <option value="100">100</option>
                <option value="101">101</option>
            </datalist >   
            <br><br>

        <h2>Garde</h2>
            <input type="number" name="Garde" min="0" max="100">
            <br><br>   

        <h2>lecturer's firstName - </h2>
            <input type="text" name="lefirstName">
            <br><br>
        <h2>lecturer's lastName -</h2>
            <input type="text" name="lelastName">
            <br><br>
        <h2>dean's FullName -</h2>
            <input type="text" name="deanFullName">
            <br><br>
        <button>check</button>
        <br><br>
    </form>
</body>
</html>